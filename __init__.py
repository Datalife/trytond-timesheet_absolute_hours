# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .res import User, get_absolute_hours_time
from .timesheet import (
    Line, HoursEmployee, HoursEmployeeWeekly, HoursEmployeeMonthly)


def register():
    Pool.register(
        User,
        Line,
        HoursEmployee,
        HoursEmployeeWeekly,
        HoursEmployeeMonthly,
        module='timesheet_absolute_hours', type_='model')
