# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


_absolute_hours_time = {
    'h': 60 * 60,
    'm': 60,
    's': 1
}


def get_absolute_hours_time():
    return _absolute_hours_time


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    @classmethod
    def _get_preferences(cls, user, context_only=False):
        res = super()._get_preferences(
            user, context_only=context_only)
        res['absolute_hours_time'] = get_absolute_hours_time()
        return res
