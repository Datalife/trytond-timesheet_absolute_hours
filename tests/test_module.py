# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class TimesheetAbsoluteHoursTestCase(ModuleTestCase):
    """Test module"""
    module = 'timesheet_absolute_hours'


del ModuleTestCase
