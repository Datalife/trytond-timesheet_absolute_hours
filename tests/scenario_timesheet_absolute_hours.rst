================================
Timesheet Abosolute hours manage
================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> today = datetime.date.today()


Install stock_unit_load_quality_control Module::

    >>> config = activate_modules('timesheet_absolute_hours')


Create company::

    >>> try:
    ...     company = get_company()
    ... except:
    ...     _ = create_company()
    ...     company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create timesheet lines::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> party = Party(name='Empleado 1')
    >>> party.save()
    >>> employee = Employee(company=company, party=party)
    >>> employee.save()
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> line = Line(employee=employee, date=today, work=work, duration=datetime.timedelta(hours=8))
    >>> line.save()
    >>> line.total_hours == 8.0
    True

Check change in duration::

    >>> str(line.duration)
    '8:00:00'
    >>> line.total_hours = 20.5
    >>> str(line.duration)
    '20:30:00'
    >>> line.save()
    >>> line.total_hours
    20.5
