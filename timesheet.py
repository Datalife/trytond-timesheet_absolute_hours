# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta

from trytond.model import fields
from trytond.pool import PoolMeta


class Line(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    total_hours = fields.Function(fields.Float('Hours'),
        'get_total_hours', setter='set_total_hours')

    @classmethod
    def set_total_hours(cls, lines, name, value):
        pass

    @fields.depends('total_hours', methods=['on_change_duration'])
    def on_change_total_hours(self, name=None):
        if self.total_hours is not None:
            self.duration = timedelta(hours=self.total_hours)
            self.on_change_duration()
        else:
            self.duration = None

    @fields.depends('duration')
    def on_change_duration(self):
        self.total_hours = self.get_total_hours()

    def get_total_hours(self, name=None):
        if self.duration:
            return self.hours
        return 0


class HoursEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.duration.converter = 'absolute_hours_time'


class HoursEmployeeWeekly(metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_weekly'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.duration.converter = 'absolute_hours_time'


class HoursEmployeeMonthly(metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_monthly'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.duration.converter = 'absolute_hours_time'
